package ru.ml.platform.server;

import io.rsocket.RSocket;
import io.rsocket.RSocketFactory;
import io.rsocket.frame.decoder.PayloadDecoder;
import io.rsocket.transport.netty.client.TcpClientTransport;
import lombok.SneakyThrows;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.rsocket.RSocketRequester;
import org.springframework.messaging.rsocket.RSocketStrategies;
import org.springframework.util.MimeTypeUtils;

@Configuration
public class SocketConfiguration {

    @Bean
    @SneakyThrows
    RSocket rSocket() {
        return RSocketFactory
                .connect()
                .metadataMimeType("message/x.rsocket.composite-metadata.v0")
                .frameDecoder(PayloadDecoder.ZERO_COPY)
                .dataMimeType(MimeTypeUtils.APPLICATION_JSON_VALUE)
                .transport(TcpClientTransport.create("127.0.0.1", 7000))
                .start()
                .block();
    }

    @Bean
    RSocketRequester requester(RSocket rSocket, RSocketStrategies strategies) {
        return RSocketRequester.wrap(rSocket, MimeTypeUtils.APPLICATION_JSON,
                MimeTypeUtils.parseMimeType("message/x.rsocket.composite-metadata.v0"),
                strategies);
    }
}
