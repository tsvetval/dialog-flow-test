package ru.ml.platform.server.controller;

import lombok.AllArgsConstructor;
import org.reactivestreams.Publisher;
import org.springframework.http.MediaType;
import org.springframework.messaging.rsocket.RSocketRequester;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import ru.ml.platform.server.dto.GreetingsRequest;
import ru.ml.platform.server.dto.GreetingsResponse;

import java.nio.ByteBuffer;

@RestController
@AllArgsConstructor
public class SocketController {
    private final RSocketRequester requester;


    @GetMapping("/greet/{name}")
    public Publisher<ByteBuffer> greet(@PathVariable String name) {
        return requester
                .route("greet")
                .data(new ByteBuffer(name))
                .retrieveMono(GreetingsResponse.class);
    }

    @GetMapping(value = "/greet-stream/{name}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Publisher<GreetingsResponse> greetStream(@PathVariable String name) {
        return requester
                .route("greet-stream")
                .data(new GreetingsRequest(name))
                .retrieveFlux(GreetingsResponse.class);
    }
}
