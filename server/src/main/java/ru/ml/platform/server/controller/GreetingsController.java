package ru.ml.platform.server.controller;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.ml.platform.server.dto.GreetingsRequest;
import ru.ml.platform.server.dto.GreetingsResponse;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.Instant;
import java.util.stream.Stream;

@Controller
public class GreetingsController {

    @MessageMapping("greet")
    Mono<GreetingsResponse> greet(ByteBuffer request) {
        return Mono.just(new GreetingsResponse("Hello " + "" + StandardCharsets.UTF_8.decode(request).toString() + " @ " + Instant.now()));
    }

    @MessageMapping("greet-stream")
    Flux<GreetingsResponse> greetStream(GreetingsRequest request) {
        return Flux.fromStream(Stream.generate(
                () -> new GreetingsResponse("Hello " + request.getName() + " @ " + Instant.now())
        )).delayElements(Duration.ofSeconds(1));
    }
}
