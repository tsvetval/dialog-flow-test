package com.example.dialogflow;

import com.google.api.client.util.Maps;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.dialogflow.v2.*;
import lombok.RequiredArgsConstructor;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
public class BotEngine {
    private final String projectId;
    private final String sessionId;
    private final String languageCode;

    private String DIALOG_STARTER = "Hi";

//    public static void main (String... args) {
//        BotEngine bot = new BotEngine();
//        bot.projectId = "testbot003proj";
//        bot.sessionId = "session_001";
//        bot.languageCode = "en-US";
//        bot.runDialog();
//    }

    public void runDialog() {
//        GoogleCredentials credentials = null;
//        try {
//            credentials = GoogleCredentials.getApplicationDefault();
//        } catch (IOException e) {
//            System.out.printf("Failed getting credentials: " + e.getMessage());
//        }
//        System.out.println(credentials);
        System.out.println("\n" + detectIntent(DIALOG_STARTER) + "\n");
    }

    private String detectIntent(String inputPhrase) {
        List<String> input = Collections.singletonList(inputPhrase);
        Map<String, QueryResult> results;
        try {
            results = detectIntentTexts(projectId, input, sessionId, languageCode);
        } catch (Exception e) {
            System.out.println("Error while retrieving intent: " + e.getMessage());
            return null;
        }
        return results.values().stream().findFirst().map(QueryResult::getFulfillmentText).orElse("No intent or fulfillment text found");
    }

    /**
     * Copied from TestMain and modified (commented out console output)
     *
     * Returns the result of detect intent with texts as inputs.
     * <p>
     * Using the same `session_id` between requests allows continuation of the conversation.
     *
     * @param projectId    Project/Agent Id.
     * @param texts        The text intents to be detected based on what a user says.
     * @param sessionId    Identifier of the DetectIntent session.
     * @param languageCode Language code of the query.
     * @return The QueryResult for each input text.
     */
    private static Map<String, QueryResult> detectIntentTexts(
            String projectId,
            List<String> texts,
            String sessionId,
            String languageCode) throws Exception {
        Map<String, QueryResult> queryResults = Maps.newHashMap();

        // Instantiates a client
        try (SessionsClient sessionsClient = SessionsClient.create()) {
            // Set the session name using the sessionId (UUID) and projectID (my-project-id)
            SessionName session = SessionName.of(projectId, sessionId);
//            System.out.println("Session Path: " + session.toString());

            // Detect intents for each text input
            for (String text : texts) {
                // Set the text (hello) and language code (en-US) for the query
                TextInput.Builder textInput = TextInput.newBuilder().setText(text).setLanguageCode(languageCode);

                // Build the query with the TextInput
                QueryInput queryInput = QueryInput.newBuilder().setText(textInput).build();

                // Performs the detect intent request
                DetectIntentResponse response = sessionsClient.detectIntent(session, queryInput);

                // Display the query result
                QueryResult queryResult = response.getQueryResult();

//                System.out.println("====================");
//                System.out.format("Query Text: '%s'\n", queryResult.getQueryText());
//                System.out.format("Detected Intent: %s (confidence: %f)\n",
//                        queryResult.getIntent().getDisplayName(), queryResult.getIntentDetectionConfidence());
//                System.out.format("Fulfillment Text: '%s'\n", queryResult.getFulfillmentText());

                queryResults.put(text, queryResult);
            }
        }
        return queryResults;
    }
}
