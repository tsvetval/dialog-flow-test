package com.example.dialogflow;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class ChatbotEngineIT {
    private BotEngine bot;

    @Before
    public void setUp() {
        bot = new BotEngine("testbot003proj","session_001","en-US");
    }

    @Test
    public void testRunDialog() {
        bot.runDialog();
    }
}
